require 'open-uri'
require 'nokogiri'
require "uri"
require "net/http"

def get_redirect_url(url)
  def get(url)
    begin
      url = URI.parse(url)
      http = Net::HTTP.get(url.host,url.path)
      http.each_line do |line|
        if /window\.location\.href/ =~ line
          return line[/".*"/][1..-2]
        end
      end
      return url
    rescue
      return url
    end
  end

  url0 = get(url)
  url1 = get(url0)
  if url0 == url1
    return url0
  else
    return get_redirect_url(url1)
  end
end

def walk(node)
  ret = []
  tmp = []
  node.children.each do |child|
    walk(child) if child.child
    if child.name == "b"
      if tmp.size > 0
        ret << tmp
        tmp = []
      end
      tmp << child
    end
    if tmp.size > 0 and child.name == "a"
      tmp << child
    end
  end

  ret.each do |nodes|
    nodes.each do |node|
      if node.name == 'a'
        url = node.attributes['href'].value
        puts get_redirect_url(url)
      else
        p node.children[0]
      end
    end
    puts ""
    return
  end
end

def get_2ch_menu
  url = 'http://www.2ch.net/bbsmenu.html'
  html = open(url, "r:sjis").read.encode("utf-8")
  doc = Nokogiri::HTML(html)
  doc.xpath('//body').each do |node|
    walk(node)
  end
end

task :get_2ch_menu do
  get_2ch_menu()
end
